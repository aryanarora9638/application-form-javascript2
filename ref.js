var http = require('http');
var express = require("express");
var app = express();
var fs = require('fs');
var path = require('path');
var url = require('url');
var qs = require('querystring');

var MongoClient = require('mongodb').MongoClient;
var murl = "mongodb://localhost:27017/";
//================================================================
var contents = fs.readFileSync('./Data/courses.json');
var obj = JSON.parse(contents);
console.log(obj[0]);


app.use(express.static(path.join(__dirname, 'public')));
app.set('views', __dirname);
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
//app.use(bodyParser.urlencoded({ extended: true }));

//document.getElementById('c_1').innerHTML = obj.name;
//================================================================
var server = http.createServer();
server.on('request', function(req,res){

  console.log(req.method,'request:', req.url);
  var urlObj = url.parse(req.url, true); // url into query object
  //console.log(urlObj.query.lname);
  if (req.method === 'GET' && req.url === '/login'){//.match(/^\/.+\.html$/)
    
    var filepath = './admin_login.html';
    console.log(filepath);
    fs.readFile(filepath, function(err, contents){
      if(err){
      	console.log('error encounter at reading login file phase')
      } else {
        res.writeHead(200, {"Content-Type": "text/html"});
        // console.log("POST request: /login");
        // console.log("./admin_login");
        // console.log("GET request: /start_check_ins");
        // console.log("./start_check_ins");
        //console.log("Course added");

        res.write(contents);
        res.end();
      }

    });

  } 

  else if( req.method === 'GET' && req.url === '/checkIn_page'){
    var filepath = './checkIn_page.html';
    console.log(filepath);
    fs.readFile(filepath, function(err, contents){
      if(err){
        console.log('error encounter at reading student Check-in file phase')
      } else {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.write(contents);
        res.end();
      }

    });
  }
  else if (req.method === 'POST' && req.url === '/checkIn_page'){

    console.log('Added Your attendence now');
    var body ='';
    req.on('data', function(data){
      body += data.toString();
    });
    req.on('end', function(){

      var postObj = qs.parse(body);
      
      var jsonObj = addUser(postObj.courseid, postObj.name , postObj.username);//users array saved here

      console.log(`Student logged in ${postObj.name} - ${postObj.username} for ${postObj.courseid}`);
      console.log('Student Data Saved');
      closeCheckIn();
      //creating and storing all the student information in a json file to access later
      var outputFile = JSON.stringify(jsonObj ,null,"\t");
      outputFile = JSON.stringify(jsonObj ,null,4);
      fs.appendFileSync("./Data/users.json" , outputFile );
      console.log('User Data Saved');
      res.end();

    });
  }
//   else if (req.method === 'GET' && req.url === '/background.jpeg'){//.match(/^\/.+\.jpg$/)

//     var imgpath  = './background.jpeg';
//     var imgstream = fs.createReadStream(imgpath, { highWaterMark: 1024 });
//     res.writeHead(200, {"Content-Type": "image/jpeg"});
//     imgstream.pipe(res);
//     console.log('Bg-Img served');

//   } 
  else if (req.method === 'POST' && req.url === '/login'){

  	console.log('Logging in now');
    var body ='';
    req.on('data', function(data){
      body += data.toString();
    });
    req.on('end', function(){
      var postObj = qs.parse(body);
      console.log(`Admin logged in ${postObj.uname} - ${postObj.password}`);
      if(postObj.uname == 'admin' && postObj.password == '1234'){
        console.log('Login Credentials checked');
      }
      console.log('User Data Saved');
      res.end();

    });
  }
  else if (req.method === 'POST' && req.url === '/start_check_ins'){

    console.log('Course checking in now');
    var body ='';
    req.on('data', function(data){
      body += data.toString();
    });
    req.on('end', function(){
      var postObj = qs.parse(body);
      console.log(`Course cheked in ${postObj.courseid} - ${postObj.lateLimit}`);
      var jsonObj = course_entered(postObj.courseid, postObj.lateLimit);
      console.log('Check-in  Data Saved');
      var filepath = './admin_landing2.html';
      console.log(filepath);

      var outputFile = JSON.stringify(jsonObj ,null,"\t");
      outputFile = JSON.stringify(jsonObj ,null,4);
      fs.writeFileSync("./Data/courses.json" , outputFile );
      console.log('User Data Saved');


      app.render('admin_landing2', {var1 : "pehla" , var2 : "dusra"});

    });

  }
  else if (req.method === 'POST' && req.url === '/stop_check_ins'){

    console.log('Stopping checkin for a course now');
    
  }

  else if(req.method === 'GET' && req.url === '/view_History'){

    //view_History();
    

  }

//   else if (req.method === 'GET' && req.url === '/feedback.html'){//.match(/^\/.+\.html$/)
//     var filepath = './feedback.html';
//     //=================================================================================//make a welcome page before this form html.
//     console.log(filepath);
//     fs.readFile(filepath, function(err, contents){
//       if(err){
//       	console.log('error encounter at reading feedback phase')
//       } else {
//         res.writeHead(200, {"Content-Type": "text/html"});
//         res.write(contents);
//         res.end();
//       }

//     });
// }
//   else if(req.method === 'GET' && req.url ==='/data/users.json'){
//   	var files = fs.readdirSync('./Data');
//   	var x = 0;
//   	console.log(files);
//   	for (var i = 0 ; i < files.length ; i++){
//   		if(files[i] == 'users.json'){
//   			x = 1;
//   		}
//   	}

//   	if(x === 1){//if file exists

//   		var filepath = './Data/users.json';
//   		console.log(`File exists, Serving file ${filepath}`);
//   		fs.readFile(filepath, function(err, contents){
//   			if(err){
//   				console.log('error encounter at getting json file phase');
//   				throw err;
//   			}
//   			else{
//   				res.writeHead(200, {"Content-Type": "text"});
//   				res.write(contents);
//   				res.end();
//   			}
//   		});
//   	}

//   	else{//file does not exist
//   				console.log('No such file in the Directory');
//   				var postObj = qs.parse(body);
//   				var outputFile = JSON.stringify(postObj ,null,"\t");

//   				fs.writeFileSync('./Data/users.json', outputFile);//{
//   				console.log('New file created');
//   				var filepath = './Data/users.json';
//   				console.log(`Now File exists, Serving file ${filepath}`);
//   				fs.readFile(filepath, function(err, contents){
//   					if(err){
//   						console.log('error encounter at getting json file phase');
//   						throw err;
//   					}
//   					else{
//   						res.writeHead(200, {"Content-Type": "text"});
//   						res.write(contents);
//   						res.end();
//   					}
//   				});
  				
  				
//   			}
  		
//   	}
//   else if(req.method === 'GET' && req.url === '/users.html'){

//   	var files = fs.readdirSync('./Data');
//   	var x = 0;
//   	console.log(files);
//   	for (var i = 0 ; i < files.length ; i++){
//   		if(files[i] == 'users.html'){
//   			x = 1;
//   		}
//   	}

//   	if(x == 1){//file exist
//   		var filepath = './Data/users.html';
//   		fs.readFile(filepath, "UTF-8",function(err,contents){
//   		if(err){
//   			console.log('error during reading json file');
//   			throw err;
//   		}
//   		else{
//   			res.writeHead(200, {"Content-Type": "text/html"});
//   			res.write(contents);
//   			res.end();
//   		}
//   	});
//   	}
  	
//   	else{//file does not exist
//   		console.log('No such file in the Directory');
//   		var filepath = './Data/users.json';
//   		var contents = fs.readFileSync(filepath, "UTF-8");
//   		console.log(contents);
//   		//console.log(array.length);
//   		console.log(contents[1].fname1);

  		


//   	}

//   	var files = fs.readdirSync('./Data');
//   	var x = 0;
//   	console.log(files);
//   	for (var i = 0 ; i < files.length ; i++){
//   		if(files[i] == 'users.json'){
//   			x = 1;
//   		}
//   	}

//   	if(x === 1){//if file exists

//   		var filepath = './Data/users.json';
//   		console.log(`File exists, Serving file ${filepath}`);
//   		fs.readFile(filepath, function(err, contents){
//   			if(err){
//   				console.log('error encounter at getting json file phase');
//   				throw err;
//   			}
//   			else{
//   				res.writeHead(200, {"Content-Type": "text"});
//   				res.write(contents);
//   				res.end();
//   			}
//   		});
//   	}
  	

//   }



//   else {
//     res.writeHead(404);
//     res.write('404 Error please check the URL carefully');
//     console.log('error ');
//     res.end()
//   }
 });
server.listen(8080);

  var c_1;//course 1
  var c_2;//course 2
  var c_3;//course 3
  var c_4;//course 4
  var c_5;//course 5
  var c_6;//course 6
  var c_7;//course 7

  var courses = [];

  var i = 0;

function course_entered(x,y){
  i++;//new course entered
    if(i <= 7){
      if(i == 1 ){
        c_1 = {"name" : x, "limit" : y};
        courses.push(c_1);
      }
      if(i == 2 ){
        c_2 = {"name" : x, "limit" : y};
        courses.push(c_2);
      }
      if(i == 3 ){
        c_3 = {"name" : x, "limit" : y};
        courses.push(c_3);
      }
      if(i == 4 ){
        c_4 = {"name" : x, "limit" : y};
        courses.push(c_4);
      }
      if(i == 5 ){
        c_5 = {"name" : x, "limit" : y};
        courses.push(c_5);
      }
      if(i == 6 ){
        c_6 = {"name" : x, "limit" : y};
        courses.push(c_6);
      }
      if(i == 7 ){
        c_7 = {"name" : x, "limit" : y};
        courses.push(c_7);
      }

      return courses;

    }
    else{
      alert('Sorry You Cannot add any more courses for the check in at this moment');
    }
}


var users = [];
var x;
function addUser(x,y,z){
  x = {"name" : x,
     "course" : y,
     "username" : z
   }

   users.push(x);
   return users;
}


var a = 'testname'

//DATABASE
//===================================================================================
function closeCheckIn(){
  MongoClient.connect(murl, function(err, client) {
  if (err){
    console.log('err while connecting');
    throw err;
  }
  var database = client.db('mydb');
  var myobj = users;
  database.collection(a).insertMany(myobj, function(err, res){
    if (err){
      console.log('finding error');
      throw err;
    }
    database.collection(a).find().forEach(function(doc){
    console.log(doc);
    });
    console.log("Number of documents inserted in the DATABASE: " + res.insertedCount);
    //db.close();
  });
});


}

function view_History(){
  MongoClient.connect(murl, function(err, client) {
  if (err){
    console.log('err while connecting');
    throw err;
  }
  var database = client.db('mydb');
  console.log('Serving all the students History')
  database.collection('asn3').find().forEach(function(doc){
    console.log(doc);
  });
});
}
//===================================================================================





console.log('Magic is happening on port 8080');

