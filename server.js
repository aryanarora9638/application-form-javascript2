var express = require("express");
var app = express();
var port = process.env.PORT || 11308;
var path = require('path');
var bodyParser = require('body-parser')
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://aryana:rcR6Fx0M@127.0.0.1:27017/cmpt218_aryana?authSource=admin";
var fs = require('fs');
var qs = require('querystring');
// const hbs = require('hbs');
// hbs.registerPartials(__dirname + './views'); 

// app.set('view engine','hbs');
// Require static assets from public folder
app.use(express.static(path.join(__dirname, 'public')));
// Set 'views' directory for any views 
// being rendered res.render()
app.set('views', __dirname);
// Set view engine as EJS
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//=======================DOUBT =================================
app.locals.yolo = require('./Data/courses.json');
//=======================DOUBT =================================






//Basic Request 
app.use('/', function(req,res,next){
  console.log(req.method, 'request:', req.url);
  next();//calls the next function based on the type of request
});

//Welcome Page

app.get('/login', function(req,res){
  res.sendFile(path.join(__dirname + '/welcome.html'));
  var filepath = './welcome.html';
  console.log(filepath);
});


app.get('/background.jpeg', function(req,res){
  res.sendFile(path.join(__dirname + '/background.jpeg'));
  var filepath = './background.jpeg';
  console.log(filepath);
});




 //Login Page [Stage 1 ]
app.get('/_login', function(req,res){
  res.sendFile(path.join(__dirname + '/admin_login.html'));
  var filepath = './admin_login.html';
  console.log(filepath);
});

app.get('/checkIn_page', function(req,res){
  res.sendFile(path.join(__dirname + '/checkIn_page.html'));
  var filepath = './checkIn_page.html';
  console.log(filepath);
});


app.post('/_login', function(req,res){
  res.redirect('_/login');
});



// app.get('/stop_check_ins', function(req,res){
//   res.sendFile(path.join(__dirname + '/admin_landing2.html'));
//   var filepath = './stop_check_ins.html';
//   console.log(filepath);
// });



//Start Check ins [Stage 2  ]
app.post('/start_check_ins',function(req,res,next){
  console.log('Course checking in now');
  console.log(`Course cheked in ${req.body.courseid} - ${req.body.lateLimit}`);

  var jsonObj = course_entered(req.body.courseid, req.body.lateLimit);
  var outputFile = JSON.stringify(jsonObj ,null,"\t");
  outputFile = JSON.stringify(jsonObj ,null,4);
  fs.writeFileSync("./Data/courses.json" , outputFile );
  console.log('Admin proposed course Data Saved');

  next()
  
},function(req,res){
  console.log('Serving Admin landing 2');
  res.render('admin_landing2', {var1 : c_1 , var2 : c_2 , var3 : c_3 , var4 : c_4 , var5 : c_5 , var6 : c_6 , var7 : c_7});

});



//Stop Check ins [Stage 3]
app.post('/stop_check_ins', function(req,res){

  console.log(`Closing Check_ins for ${req.body.stopping}` );
  if(check_course(req.body.stopping)){
    var x = checkCourse(req.body.stopping);//x se array change //req.body.stopping se collection name
    assign_collection(x,req.body.stopping);
    console.log(collection_name);
    console.log(myobj);
    console.log('Pushing Data in Database');
    closeCheckIn();//saving data in collection/database
    console.log('Database Saved');
    course_removed(req.body.stopping);//make c_i, users_i level up
    console.log('Course removed');
    res.redirect('/login');
    
  }
  else{
    console.log('Course is already closed');//
    res.render('closed',{var1 : req.body.stopping});
  }
  
});

app.post('/checkIn_page', function(req,res){

  console.log('Checking In a student');
  console.log(`Checking in ${req.body.name} as ${req.body.username} for ${req.body.courseid}`);

  console.log('Checking if course is still open');
  if(check_course(req.body.courseid)){

    console.log('Verifying If user has already checked in');

    if(verify(req.body.courseid,req.body.username)){
    
      console.log('Confirmed both, Open');
      var jsonObj = addUser(req.body.courseid, req.body.name , req.body.username);//based on course user is pushed to an array

      var outputFile = JSON.stringify(jsonObj ,null,"\t");
      outputFile = JSON.stringify(jsonObj ,null,4);
      fs.appendFileSync("./Data/users.json" , outputFile );
      res.render('thanks', {var1: req.body.courseid , var2 : req.body.name , var3 : req.body.username });
    }
    else{//user already added himself
      console.log('User has already checked in');
      res.render('already',{var1: req.body.courseid , var2 : req.body.name , var3 : req.body.username });
    }

    
  }
  else{
    console.log('Unfortunately, the Admin has closed the check_ins');
    res.render('nothanks', {var1: req.body.courseid , var2 : req.body.name , var3 : req.body.username });

  }


});




// app.get('/yolo',function(req,res){
//   //res.sendFile(path.join(__dirname + '/admin_landing2.hbs'));
//   res.render('admin_landing2', {var1 : "pehla" , var2 : "dusra"});
// });

app.listen(port);
console.log("App Running On 11308");


//=========================================================


  var c_1 = {"name" : "-", "limit" : "-"};//course 1
  var c_2 = {"name" : "-", "limit" : "-"};//course 2
  var c_3 = {"name" : "-", "limit" : "-"};//course 3
  var c_4 = {"name" : "-", "limit" : "-"};//course 4
  var c_5 = {"name" : "-", "limit" : "-"};//course 5
  var c_6 = {"name" : "-", "limit" : "-"};//course 6
  var c_7 = {"name" : "-", "limit" : "-"};//course 7

  var courses = [];

  var i = 0;

function course_entered(x,y){
  i++;//new course entered
    if(i <= 7){
      if(i == 1 ){
        c_1 = {"name" : x, "limit" : y};
        courses.push(c_1);
      }
      if(i == 2 ){
        c_2 = {"name" : x, "limit" : y};
        courses.push(c_2);
      }
      if(i == 3 ){
        c_3 = {"name" : x, "limit" : y};
        courses.push(c_3);
      }
      if(i == 4 ){
        c_4 = {"name" : x, "limit" : y};
        courses.push(c_4);
      }
      if(i == 5 ){
        c_5 = {"name" : x, "limit" : y};
        courses.push(c_5);
      }
      if(i == 6 ){
        c_6 = {"name" : x, "limit" : y};
        courses.push(c_6);
      }
      if(i == 7 ){
        c_7 = {"name" : x, "limit" : y};
        courses.push(c_7);
      }

      return courses;

    }
    else{
      alert('Sorry You Cannot add any more courses for the check in at this moment');
    }
}

function course_removed(course){
  i--;
  if(c_1.name == course){
    c_1 = c_2;
    c_2 = c_3;
    c_3 = c_4;
    c_5 = c_6;
    c_6 = c_7;
    c_7.name = "-"; c_7.limit = "-";
    users_1 = users_2;
    users_2 = users_3;
    users_3 = users_4;
    users_4 = users_5;
    users_5 = users_6;
    users_6 = users_7;
    users_7 = [];

  }
  else if(c_2.name == course){
    c_2 = c_3;
    c_3 = c_4;
    c_4 = c_5;
    c_5 = c_6;
    c_6 = c_7;
    c_7.name = "-"; c_7.limit = "-";
    users_2 = users_3;
    users_3 = users_4;
    users_4 = users_5;
    users_5 = users_6;
    users_6 = users_7;
    users_7 = [];
  }
  else if(c_3.name == course){
    c_3 = c_4;
    c_4 = c_5;
    c_5 = c_6;
    c_6 = c_7;
    c_7.name = "-"; c_7.limit = "-";
    users_3 = users_4;
    users_4 = users_5;
    users_5 = users_6;
    users_6 = users_7;
    users_7 = [];
  }
  else if(c_4.name == course){
    c_4 = c_5;
    c_5 = c_6;
    c_6 = c_7;
    c_7.name = "-"; c_7.limit = "-";
    users_4 = users_5;
    users_5 = users_6;
    users_6 = users_7;
    users_7 = [];
  }
  else if(c_5.name == course){
    c_5 = c_6;
    c_6 = c_7;
    c_7.name = "-"; c_7.limit = "-";
    users_5 = users_6;
    users_6 = users_7;
    users_7 = [];
  }
  if(c_6.name == course){
    c_6 = c_7;
    c_7.name = "-"; c_7.limit = "-";
    users_6 = users_7;
    users_7 = [];
  }
  if(c_7.name == course){
    c_7.name = "-"; c_7.limit = "-";
    users_7 = [];
  }
}

function check_course(x){
  if(c_1.name == x || c_2.name == x || c_3.name == x || c_4.name == x || c_5.name == x || c_6.name == x || c_7.name == x){
    return true;
  }else{
    return false;
  }
}


var users_1 = [];
var users_2 = [];
var users_3 = [];
var users_4 = [];
var users_5 = [];
var users_6 = [];
var users_7 = [];

var g;

function addUser(x,y,z){
  g = {"name" : y,
     "course" : x,
     "username" : z
   }

  var check = checkCourse(g.course);


  if(check == 1 ){
    users_1.push(g);
   return users_1;
  }
  if(check == 2 ){
    users_2.push(g);
   return users_2;
  }
  if(check == 3 ){
    users_3.push(g);
   return users_3;
  }
  if(check == 4 ){
    users_4.push(g);
   return users_4;
  }
  if(check == 5 ){
    users_5.push(g);
   return users_5;
  }
  if(check == 6 ){
    users_6.push(g);
   return users_6;
  }
  if(check == 7 ){
    users_7.push(g);
   return users_7;
  }

   
}

function checkCourse(x){
  if(x == c_1.name){return 1;}
  else if(x == c_2.name){return 2;}
  else if(x == c_3.name){return 3;}
  else if(x == c_4.name){return 4;}
  else if(x == c_5.name){return 5;}
  else if(x == c_6.name){return 6;}
  else if(x == c_7.name){return 7;}
  else{
    return 0;
  }
}

function verify(x,y){
  var check = checkCourse(x);
  var array;
  if(check==1){array = users_1;}
  else if(check==2){array = users_2;}
  else if(check==3){array = users_3;}
  else if(check==4){array = users_4;}
  else if(check==5){array = users_5;}
  else if(check==6){array = users_6;}
  else if(check==7){array = users_7;}
  var vv = 0;
  //console.log(array);
  //console.log(y);
  array.forEach(function(element){
    if(element.username == y){
      //console.log(element);
      vv = 1;
    }
  });

  if(vv == 1){
    return false;
  }
  else{
    return true;
  }
  

}
var myobj;
var collection_name;
function assign_collection(x,y){
  if(x==1){myobj = users_1; collection_name = y;}
  if(x==2){myobj = users_2; collection_name = y;}
  if(x==3){myobj = users_3; collection_name = y;}
  if(x==4){myobj = users_4; collection_name = y;}
  if(x==5){myobj = users_5; collection_name = y;}
  if(x==6){myobj = users_6; collection_name = y;}
  if(x==7){myobj = users_7; collection_name = y;}
}



//DATABASE
//===================================================================================
function closeCheckIn(){

  MongoClient.connect(url, function(err, client) {
  if (err){
    console.log('err while connecting');
    throw err;
  }
  console.log('Connected successfully to server');
  var database = client.db('cmpt218_aryana');
  
  database.collection(collection_name).insertMany(myobj, function(err, res){
    if (err){
      console.log('finding error');
      throw err;
    }
    database.collection(collection_name).find().forEach(function(doc){
    console.log(doc);
    });
    console.log("Number of documents inserted: " + res.insertedCount);
    //db.close();
  });
});

}
//===================================================================================

